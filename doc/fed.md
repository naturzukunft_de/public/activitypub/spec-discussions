[[_TOC_]]
# Delivery
## Situation 1
![situation1](doc/img/fed1.png)
1. `http://servera/Jane` creates a `http://servera/note1` to `http://serverb/Max` with her AP-Client-App
2. (C2S)  The AP-Client-App sends `http://servera/note1` to `http://servera/Jane/outbox`  
3. saving in AP-Server
   - `http://servera` wraps `http://servera/note1` into a activity `http://servera/create1`  
   - `http://servera`saves `http://servera/create1` and `http://servera/note1` in `http://servera/Jane/outbox`
4. (S2S) `http://servera` delivers `http://servera/create1` and `http://servera/note1` to `http://serverb/Max/inbox`
 
### Questions
- How can Max's AP-Client-App no show Max's inbox, respectively how can `http://serverb/Max/inbox` return data to Max's AP-Client-App.   
   Max's AP-Client-App or `http://serverb` has to read data from `http://servera/Jane/outbox`   
   Expect `http://serverb` has a copy of `http://servera/note1` and `http://servera/create1`  
   And both are not authenticated to read data from `http://servera/Jane/outbox`
